const request = require('superagent')
const baseURL = 'localhost:3000/users'
describe('Backend API', function() {
  it ('should return the list of users', function (done) {
   request(baseURL, function (error, response){
          expect(response.status).toBe(200)
          expect(response.body[0].username).toBe('kiarie')
          done()
      })
  })
  it('should insert one user', (done) => {
      request.post(baseURL+'')
      .set('Accept', 'application/json')
      .send({username: 'john Doe', branch_id: 1})
      .end(function (error, response){
            expect(response.status).toBe(201)
            expect(response.body.text).toBe(undefined)
         done()   
      })
       request.get(baseURL)
       .end(function(error, response){
           expect(response.status).toBe(200)
           expect(response.body.slice(-1).pop().username).toBe('john Doe')
          done() 
       })
       
   })
//    it('should reject insert one user with invalid parameters', (done) => {
//       request.post(baseURL+'')
//       .set('Accept', 'application/json')
//       .send({username: 'john Doez', branch_id: -1})
//       .end(function (error, response){
//             expect(response.status).not.toBe(201)//for now this fails
//             expect(response.body.text).toBe('invalid branch id')
//          done()   
//       })
             
//    })
 
  it('should delete one user by id', (done) => {
      request.post(baseURL)
       .set('Accept', 'application/json')
        .type('json')
        .send({id: 200, username: 'kui Mwakesi', branch_id : '24'})
        .end((error, response)=>{
            response.body
            done()
         })
          
      request.delete(`${baseURL}/200`)
         .end((error, response) => {
         expect(response.status).toBe(200)
            done()
       })
}) 

        
});
describe('for individual records', ()=>{
      it('should get one user', (done) =>{
      request.get(baseURL+'/2')
       .end(function(error, response){
           expect(response.status).toBe(200)
           expect(response.body.username).toBe('Tali')
           expect(response.body.branch_id).toBe(24)
          done() 
       })
       
  })  

   beforeAll((done) => {
    request.patch(baseURL+'/2')
     .set('Accept', 'application/json')
      .type('json')
      .send({username: 'Tali', branch_id : '24'})
       .end(function(error, response){
           expect(response.status).toBe(200)      
       })


    
       
       request.get(baseURL+'/292929')
       .end(function(error, response){
           expect(response.status).not.toBe(200)
           expect(response.status).toBe(404)

       done()
       })
   }) 
      
  
});